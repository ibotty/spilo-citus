FROM registry.opensource.zalan.do/acid/spilo-15

RUN curl -sSL https://install.citusdata.com/community/deb.sh | bash \
 && curl -sSLo /etc/apt/trusted.gpg.d/ibotty-postgresql-public.asc https://ibotty.github.io/postgresql-related-packages/ibotty-postgresql-public.asc \
 && curl -sSLo /etc/apt/sources.list.d/ibotty-postgresql-public.list https://ibotty.github.io/postgresql-related-packages/ibotty-postgresql-public.list \
 && apt-get update \
 && apt-get install -y awscli zstd \
                       pg-ivm-pg15 \
                       pg-financial-pg15 \
                       pg-uuidv7-pg15 \
                       postgresql-14-citus-11.2 postgresql-15-citus-11.2 \
                       postgresql-15-rum \
                       supabase-wrappers-pg15 \
                       pg-row-hashes-pg15 \
 && apt-get reinstall -y python3-docutils \
 && apt-get clean
